import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-office-time',
  templateUrl: './office-time.component.html',
  styleUrls: ['./office-time.component.scss']
})
export class OfficeTimeComponent implements OnInit {

  officeTimeForm: FormGroup;

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.setOfficeTimeForm();
  }

  get salesReps$() {
    return this.dataService.getSampleSalesReps();
  }

  setOfficeTimeForm() {
    const officeTimeForm = this.formBuilder.group({
      dealerForms: this.formBuilder.array(this.getDealerFormConfigs())
    });
    this.officeTimeForm = officeTimeForm;
  }

  getDealerFormConfigs() {
    const dealerFormConfigs = [];
    const dealerFormConfig = {
      repCode: [''],
      dealerCode: [''],
      businessMeetingSheet: [''],
      brandReport: [''],
      pickReport: [''],
      cummulativeProgramReport: ['']
    };
    for (let i = 0; i < 5; i++) {
      dealerFormConfigs.push({ ...dealerFormConfig });
    }
    return dealerFormConfigs;
  }

}
