import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfficeTimeComponent } from './office-time.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [OfficeTimeComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [OfficeTimeComponent]
})
export class OfficeTimeModule { }
