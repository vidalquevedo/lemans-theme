import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RepToolsComponent } from './rep-tools.component';
import { NavbarRepToolsModule } from '../navbar-rep-tools/navbar-rep-tools.module';
import { OfficeTimeModule } from '../office-time/office-time.module';

@NgModule({
  declarations: [RepToolsComponent],
  imports: [
    CommonModule,
    NavbarRepToolsModule,
    OfficeTimeModule
  ],
  exports: [RepToolsComponent]
})
export class RepToolsModule { }
