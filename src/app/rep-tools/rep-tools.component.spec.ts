import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepToolsComponent } from './rep-tools.component';

describe('RepToolsComponent', () => {
  let component: RepToolsComponent;
  let fixture: ComponentFixture<RepToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
