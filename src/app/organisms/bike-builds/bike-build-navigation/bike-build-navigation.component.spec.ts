import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeBuildNavigationComponent } from './bike-build-navigation.component';

describe('BikeBuildNavigationComponent', () => {
  let component: BikeBuildNavigationComponent;
  let fixture: ComponentFixture<BikeBuildNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeBuildNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeBuildNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
