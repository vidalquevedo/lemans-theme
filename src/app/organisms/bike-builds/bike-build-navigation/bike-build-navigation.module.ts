import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikeBuildNavigationComponent } from './bike-build-navigation.component';

@NgModule({
  declarations: [BikeBuildNavigationComponent],
  imports: [
    CommonModule
  ],
  exports: [BikeBuildNavigationComponent] 
})
export class BikeBuildNavigationModule { }
