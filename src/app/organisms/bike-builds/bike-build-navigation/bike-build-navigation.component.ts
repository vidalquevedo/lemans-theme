import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bike-build-navigation',
  templateUrl: './bike-build-navigation.component.html',
  styleUrls: ['./bike-build-navigation.component.scss']
})
export class BikeBuildNavigationComponent implements OnInit {

  @Input() bikeBuild: any;
  highLightedLabel: any;

  constructor() { }

  ngOnInit() {
  }

  setHighlightedLabel(label) {
    if (!this.highLightedLabel) {
      this.highLightedLabel = label;
    } else {
      if (this.highLightedLabel.displayName === label.displayName) { 
        return;
      } else {
        this.highLightedLabel = label;
      }
    }
  }

  removeHighlightedLabel(label) {
    this.highLightedLabel = undefined;
  }
}
