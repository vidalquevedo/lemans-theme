import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bike-summary',
  templateUrl: './bike-summary.component.html',
  styleUrls: ['./bike-summary.component.scss']
})
export class BikeSummaryComponent implements OnInit {
  
  @Input() bikeBuild: any;

  constructor() { }

  ngOnInit() {
  }

}
