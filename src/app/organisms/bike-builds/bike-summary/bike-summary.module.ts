import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikeSummaryComponent } from './bike-summary.component';

@NgModule({
  declarations: [BikeSummaryComponent],
  imports: [
    CommonModule
  ],
  exports: [BikeSummaryComponent]

})
export class BikeSummaryModule { }
