import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganismsRoutingModule } from './organisms-routing.module';
import { OrganismsComponent } from './organisms.component';
import { PartBadgeModule } from '../part-badge/part-badge.module';
import { CarouselHeroModule } from '../carousel-hero/carousel-hero.module';
import { CarouselProductModule } from '../carousel-product/carousel-product.module';
import { NavbarHeaderModule } from '../navbar-header/navbar-header.module';
import { BikeSummaryModule } from './bike-builds/bike-summary/bike-summary.module';
import { BikeBuildNavigationModule } from './bike-builds/bike-build-navigation/bike-build-navigation.module';

@NgModule({
  declarations: [OrganismsComponent],
  imports: [
    CarouselHeroModule,
    CommonModule,
    NavbarHeaderModule,
    OrganismsRoutingModule,
    PartBadgeModule,
    CarouselProductModule,
    BikeSummaryModule,
    BikeBuildNavigationModule
  ]
})
export class OrganismsModule { }
