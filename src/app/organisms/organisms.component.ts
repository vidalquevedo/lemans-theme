import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-organisms',
  templateUrl: './organisms.component.html',
  styleUrls: ['./organisms.component.scss']
})
export class OrganismsComponent implements OnInit {

  bikeBuilds = [
    {
      name: '2017 FLHRXS Road King Special by Suburban Motors H-D®',
      description: 'Built by Suburban Motors H-D, this 2017 FLHRXS Road King Special features an array of top notch parts from some of the top brands in the industry. All of which can be found in the Fatbook and from your local dealer.',
      textSide: 'right',
      slug: '2017-flhrxs-road-king-special-by-suburban-motors-h-d',
      summary: {
        images: {
          desktop: {
            height: 100,
            width: 100,
            url: '/assets/images/bike-builds/desktop/bike_builds-bike_ban-desktop-01.jpg'
          },
          mobile: {
            height: 100,
            width: 100,
            url: '/assets/images/bike-builds/mobile/bike_builds-bike_ban-mobile-01.jpg'
          }
        }
      },
      sections: [
        {
          title: 'brakes',
          parts: [
            '1601-0476',
            '1602-1152',
            '1701-0635',
            '0701-0636',
            '0701-0637',
            '0701-0638'
          ]
        },
        {
          title: 'chassis',
          parts: [
            '0213-0733',
            '0411-0121',
            '0214-0357',
            '0713-0142',
            '3501-1078',
            '3501-1082',
            '0414-0543',
            '0306-0304',
            '0306-0536',
            '0214-1763',
            '1310-1847',
            '3501-0757',
            '0801-1052'
          ]
        },
        {
          title: 'driveline',
          parts: [
            '0710-0240',
            '1107-0621',
            '1130-0312'
          ]
        },
        {
          title: 'engine',
          parts: [
            '0928-0090',
            '0940-1661',
            '0940-1663',
            '0933-0091',
            '2401-1017',
            '1801-1206',
            '1802-0391',
            'N/A'
          ]
        },
        {
          title: 'electrical & lightning',
          parts: [
            '2104-0326',
            '2050-0223',
            '2001-1747',
            '2020-1590',
            '2040-2268',
            '2001-1264',
            '2040-2336',
            '2210-0509',
            '2103-0359',
            '2113-0322',
            '2107-0215'
          ]
        },
        {
          title: 'fuel & oil',
          parts: [
            '1014-0289',
            '1010-2407',
            '1020-2203'
          ]
        },
        {
          title: 'handlebars & controls',
          parts: [
            '0630-0481',
            '0601-4452',
            '1620-1881',
            '1621-0851',
            '0610-2046',
            '0640-0774',
            '0640-0775'
          ]
        }
      ],
      navigation: {
        images: {
          desktop: {
            height: 600,
            width: 1140,
            url: '/assets/images/bike-builds/desktop/bike_nav_header_img-01.jpg'
          }
        },
        labels: [
          {
            displayName: '1',
            section: 'brakes',
            x: 745,
            y: 450
          },
          {
            displayName: '2',
            section: 'chassis',
            x: 635,
            y: 305
          },
          {
            displayName: '3',
            section: 'driveline',
            x: 430,
            y: 425
          },
          {
            displayName: '4',
            section: 'engine',
            x: 555,
            y: 405
          },
          {
            displayName: '5',
            section: 'electric & lightning',
            x: 700,
            y: 245
          },
          {
            displayName: '6',
            section: 'fuel & oil',
            x: 555,
            y: 280
          },
          {
            displayName: '7',
            section: 'handlebars & controls',
            x: 600,
            y: 175
          }
        ]
      },
    },
    {
      name: '2018 FLHXS Street Glide Special by Suburban Motors H-D®',
      description: 'Suburban Motors dove into the 2019 FatBook to create another incredible custom. This ‘18 FLHRXS Street Glide features incredible parts from some of the best brands around.',
      textSide: 'left',
      slug: '2018-flhxs-street-glide-special-by-suburban-motors-h-d',
      summary: {
        images: {
          desktop: {
            height: 100,
            width: 100,
            url: '/assets/images/bike-builds/desktop/bike_builds-bike_ban-desktop-02.jpg'
          },
          mobile: {
            height: 100,
            width: 100,
            url: '/assets/images/bike-builds/mobile/bike_builds-bike_ban-mobile-02.jpg'
          }
        }
      },
      navigation: {
        images: {
          desktop: {
            height: 600,
            width: 1140,
            url: '/assets/images/bike-builds/desktop/bike_nav_header_img-02.jpg'
          },
          mobile: {
            url: '/assets/images/bike-builds/mobile/bike_nav_header_img-mobile-02.jpg'
          }
        },
        labels: [
          {
            displayName: 1,
            section: 'brakes',
            x: 745,
            y: 450
          },
          {
            displayName: 2,
            section: 'chassis',
            x: 635,
            y: 305
          },
          {
            displayName: 3,
            section: 'driveline',
            x: 430,
            y: 425
          },
          {
            displayName: 4,
            section: 'engine',
            x: 555,
            y: 405
          },
          {
            displayName: 5,
            section: 'electrical & engine',
            x: 700,
            y: 245
          },
          {
            displayName: 6,
            section: 'fuel & oil',
            x: 555,
            y: 280
          },
          {
            displayName: 7,
            section: 'handlebards & controls',
            x: 600,
            y: 175
          },
          {
            displayName: 8,
            section: 'windshields',
            x: 635,
            y: 140
          }
        ]
      }
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
