import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartBadgeComponent } from './part-badge.component';
import { ImgModule } from '../img/img.module';

@NgModule({
  declarations: [PartBadgeComponent],
  imports: [
    CommonModule,
    ImgModule
  ],
  exports: [PartBadgeComponent]
})
export class PartBadgeModule { }
