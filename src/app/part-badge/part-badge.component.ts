import { Component, OnInit, Input, ElementRef, Renderer2 } from '@angular/core';

declare const $: any;

@Component({
  selector: 'app-part-badge',
  templateUrl: './part-badge.component.html',
  styleUrls: ['./part-badge.component.scss']
})
export class PartBadgeComponent implements OnInit {

  @Input() partStatus: string;
  @Input() layout: string;
  @Input() isPartInCurrentOrder: boolean;
  @Input() inCurrentOrder: boolean;
  id: string;
  data = {
    months: [],
    fitments: []
  };
  constructor(
      ) { }

  ngOnInit() {
    // tslint:disable-next-line:no-bitwise
    this.id = `0${((Math.random() * 46656) | 0).toString(36).slice(-3)}`;

    this.data = {
      months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      fitments: [
        { year: '2008-2018', make: 'Suzuki', model: 'GSX 1300 R Hayabusa', partNumber: '18102667'},
        { year: '2015-2016', make: 'Yamaha', model: 'YZF-R1 S', partNumber: '18102414'},
        { year: '2017', make: 'Honda', model: 'CBR 1000 RR SP', partNumber: '18102523'},
        { year: '2015-2016', make: 'Yamaha', model: 'YZF-R1 M', partNumber: '18202414'}
      ]
    };
  }

  togglePopover($event: Event, action: string) {
    $($event.target).popover(action);
  }
}
