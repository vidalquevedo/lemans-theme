import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PartCardMobileModule } from './part-card-mobile/part-card-mobile.module';
import { OrderPartsTableComponent } from './order-parts-table/order-parts-table.component';
import { FormsModule } from '@angular/forms';
import { PartBadgeModule } from './part-badge/part-badge.module';
import { RouterModule } from '@angular/router';
import { DataService } from './services/data.service';

@NgModule({
  declarations: [
    AppComponent,
    OrderPartsTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PartCardMobileModule,
    FormsModule,
    PartBadgeModule,
    RouterModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
