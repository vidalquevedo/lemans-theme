import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplatesRoutingModule } from './templates-routing.module';
import { TemplatesComponent } from './templates.component';
import { PartBadgeModule } from '../part-badge/part-badge.module';
import { RepToolsModule } from '../rep-tools/rep-tools.module';
import { SearchResultsModule } from '../search-results/search-results.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { MobileOrderDetailsModule } from '../mobile/mobile-order-details/mobile-order-details.module';
import { PackageFeedbackModule } from '../package-feedback/package-feedback.module';

@NgModule({
  declarations: [TemplatesComponent],
  imports: [
    CommonModule,
    TemplatesRoutingModule,
    PartBadgeModule,
    RepToolsModule,
    SearchResultsModule,
    DashboardModule,
    MobileOrderDetailsModule,
    PackageFeedbackModule
  ]
})
export class TemplatesModule { }
