import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar-header',
  templateUrl: './navbar-header.component.html',
  styleUrls: ['./navbar-header.component.scss']
})
export class NavbarHeaderComponent implements OnInit {

  isLoggedIn = true;
  userType: 'DEALER' | 'SALES_REP';
  constructor() { }

  ngOnInit() {
  }

  toggleIsLoggedIn() {
    this.isLoggedIn = !this.isLoggedIn;
  }

}
