import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarHeaderComponent } from './navbar-header.component';
import { NavbarRepToolsModule } from '../navbar-rep-tools/navbar-rep-tools.module';

@NgModule({
  declarations: [NavbarHeaderComponent],
  imports: [
    CommonModule,
    NavbarRepToolsModule
  ],
  exports: [NavbarHeaderComponent]
})
export class NavbarHeaderModule { }
