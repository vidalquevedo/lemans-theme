import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isEditWarehouseMode: boolean;
  isMergeMode: boolean = false;
  title = 'lemans-theme';


  listenToggleShowManageWarehouseTable(show: boolean) {
    console.log(show);
    this.isEditWarehouseMode = show;
  }

  listenToggleMergeOrders(mergeMode: boolean) {
    this.isMergeMode = mergeMode;
  }
}
