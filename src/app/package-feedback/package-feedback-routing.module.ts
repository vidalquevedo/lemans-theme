import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PackageFeedbackComponent } from './package-feedback.component';

const routes: Routes = [
	{
		path: '',
		data: { title: 'Package Feedback' },
		component: PackageFeedbackComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PackageFeedbackRoutingModule { }
