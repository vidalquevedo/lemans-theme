import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-package-feedback',
	templateUrl: './package-feedback.component.html',
	styleUrls: ['./package-feedback.component.scss']
})
export class PackageFeedbackComponent {

	stars = new FormControl('');
	stars2 = new FormControl('');

	form: FormGroup;
	form2: FormGroup;
	constructor(fb: FormBuilder) {
		this.form = fb.group({
			stars: ''
		});
		this.form2 = fb.group({
			stars2: ''
		});
	}

}
