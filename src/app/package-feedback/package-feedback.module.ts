import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PackageFeedbackComponent } from './package-feedback.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [PackageFeedbackComponent],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule
	],
	exports: [
		PackageFeedbackComponent
	]
})
export class PackageFeedbackModule { }
