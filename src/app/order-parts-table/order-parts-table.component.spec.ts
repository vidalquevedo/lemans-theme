import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPartsTableComponent } from './order-parts-table.component';

describe('OrderPartsTableComponent', () => {
  let component: OrderPartsTableComponent;
  let fixture: ComponentFixture<OrderPartsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPartsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPartsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
