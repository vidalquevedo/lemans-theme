import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-parts-table',
  templateUrl: './order-parts-table.component.html',
  styleUrls: ['./order-parts-table.component.scss']
})
export class OrderPartsTableComponent implements OnInit {
  @Input() isEditWarehouseMode: boolean;

  constructor() { }

  ngOnInit() {
  }

}
