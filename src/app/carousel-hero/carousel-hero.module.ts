import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselHeroComponent } from './carousel-hero.component';

@NgModule({
  declarations: [
    CarouselHeroComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [CarouselHeroComponent]
})
export class CarouselHeroModule { }
