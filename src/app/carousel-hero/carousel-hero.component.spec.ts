import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselHeroComponent } from './carousel-hero.component';

describe('CarouselHomeComponent', () => {
  let component: CarouselHeroComponent;
  let fixture: ComponentFixture<CarouselHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselHeroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
