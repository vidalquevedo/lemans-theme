import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-part-card-mobile',
  templateUrl: './part-card-mobile.component.html',
  styleUrls: ['./part-card-mobile.component.scss']
})
export class PartCardMobileComponent implements OnInit {
  @Input() readonly: boolean;

  constructor() { }

  ngOnInit() {
  }

}
