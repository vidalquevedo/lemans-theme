import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartCardMobileComponent } from './part-card-mobile.component';

describe('PartCardMobileComponent', () => {
  let component: PartCardMobileComponent;
  let fixture: ComponentFixture<PartCardMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartCardMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartCardMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
