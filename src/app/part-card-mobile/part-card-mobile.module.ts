import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartCardMobileComponent } from './part-card-mobile.component';
import { OrderHeaderModule } from '../order-header/order-header.module';

@NgModule({
  declarations: [PartCardMobileComponent],
  imports: [
    CommonModule, OrderHeaderModule
  ],
  exports: [
    PartCardMobileComponent
  ]
})
export class PartCardMobileModule { }
