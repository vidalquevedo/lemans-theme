import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AtomsRoutingModule } from './atoms-routing.module';
import { AtomsComponent } from './atoms.component';

@NgModule({
  declarations: [AtomsComponent],
  imports: [
    CommonModule,
    AtomsRoutingModule
  ],
  exports: [AtomsComponent]
})
export class AtomsModule { }
