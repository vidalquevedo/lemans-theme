import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtomsComponent } from './atoms.component';

const routes: Routes = [
  {
    path: '',
    component: AtomsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtomsRoutingModule { }
