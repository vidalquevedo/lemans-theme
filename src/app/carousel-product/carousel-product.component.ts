import { Component, OnInit, ViewChild, Renderer2, Input} from '@angular/core';
import { ComponentFactoryResolver } from '@angular/core/src/render3';

@Component({
  selector: 'app-carousel-product',
  templateUrl: './carousel-product.component.html',
  styleUrls: ['./carousel-product.component.scss']
})
export class CarouselProductComponent implements OnInit {

  @Input() images: string[];
  @ViewChild('scrollWindow') scrollWindow: any;
  @ViewChild('range') range: any;
  
  ngOnInit(){
    this.images = ["https://via.placeholder.com/175x175/009059/FFFFFF",
                  "https://via.placeholder.com/175x175/03256C/FFFFFF",
                  "https://via.placeholder.com/175x175/F7CB15/FFFFFF",
                  "https://via.placeholder.com/175x175/e03a3e/FFFFFF",
                  "https://via.placeholder.com/175x175/EFEFEF/000000",
                  "https://via.placeholder.com/175x175/009059/FFFFFF",
                  "https://via.placeholder.com/175x175/03256C/FFFFFF",
                  "https://via.placeholder.com/175x175/F7CB15/FFFFFF",
                  "https://via.placeholder.com/175x175/e03a3e/FFFFFF",
                  "https://via.placeholder.com/175x175/EFEFEF/000000",
                  ]
  }

  constructor(
    private renderer: Renderer2
  ) { }

  scrollRangeUpdate(e) {
    let position = e * (this.scrollWindow.nativeElement.scrollWidth - this.scrollWindow.nativeElement.offsetWidth) / this.range.nativeElement.max;
    this.renderer.setProperty(this.scrollWindow.nativeElement, 'scrollLeft', position);
  }

  slideRight(){

    let amountToMove;
    let position;

    if(this.scrollWindow.nativeElement.offsetWidth <= 500)
      amountToMove = 10.2;
    else if(this.scrollWindow.nativeElement.offsetWidth <= 510)
      amountToMove = 27;
    else
      amountToMove = 20;

    if(this.range.nativeElement.valueAsNumber + amountToMove > this.range.nativeElement.max)
      amountToMove = this.range.nativeElement.max - this.range.nativeElement.valueAsNumber;
    
    position = this.range.nativeElement.valueAsNumber + amountToMove;
    this.scrollRangeUpdate(position);
    this.renderer.setProperty(this.range.nativeElement, 'valueAsNumber', position);
  }

  slideLeft(){
    
    let amountToMove = -20;
    let position;

    if(this.range.nativeElement.valueAsNumber + amountToMove < this.range.nativeElement.min)
      amountToMove = (this.range.nativeElement.min + this.range.nativeElement.valueAsNumber)*-1;

    position = this.range.nativeElement.valueAsNumber + amountToMove;
    this.scrollRangeUpdate(position);
    this.renderer.setProperty(this.range.nativeElement, 'valueAsNumber', position);
  }

  detectScrollSpot(){
    // console.log(this.scrollWindow.nativeElement.scrollLeft);
    let position = this.scrollWindow.nativeElement.scrollLeft;
    position = position / this.scrollWindow.nativeElement.offsetWidth;
    position = position * this.range.nativeElement.max;
    // console.log(position);
    this.renderer.setProperty(this.range.nativeElement, 'valueAsNumber', position);
  }

}
