import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselProductComponent } from './carousel-product.component';
import { PartBadgeModule } from '../part-badge/part-badge.module';

@NgModule({
  declarations: [CarouselProductComponent],
  imports: [
    CommonModule,
    PartBadgeModule
  ],
  exports: [CarouselProductComponent]
})
export class CarouselProductModule { }
