import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  featuredProducts: string[];
  nutsAndNotes: string[];
  legal: string[];
  backOrders: Backorder[] = [];
  openOrders: Openorder[] = [];
  submittedOrders: Submittedorder[] = [];
  monthlySpecials: Monthlyspecial[] = [];
  newPrograms: Program[] = [];
  expiringPrograms: Program[] = [];
  cummulativePrograms: CummulativeProgram[] = [];

  constructor() { }

  ngOnInit() {
    // this.featuredProducts = [
    //   'https://dummyimage.com/300x200/fff/aaa',
    //   'https://dummyimage.com/300x200/fff/aaa',
    //   'https://dummyimage.com/300x200/fff/aaa',
    //   'https://dummyimage.com/300x200/fff/aaa'
    // ];

    // this.nutsAndNotes = [
    //   'https://dummyimage.com/300x150/fff/aaa',
    //   'https://dummyimage.com/300x150/fff/aaa',
    //   'https://dummyimage.com/300x150/fff/aaa',
    //   'https://dummyimage.com/300x150/fff/aaa'
    // ];

    // this.legal = [
    //   'https://dummyimage.com/300x150/fff/aaa',
    //   'https://dummyimage.com/300x150/fff/aaa',
    //   'https://dummyimage.com/300x150/fff/aaa',
    //   'https://dummyimage.com/300x150/fff/aaa'
    // ];

    this.featuredProducts = [
      'assets/dashboard-images/1.jpg',
      'assets/dashboard-images/2.jpg',
      'assets/dashboard-images/3.jpg',
      'assets/dashboard-images/4.jpg'
    ];

    this.nutsAndNotes = [
      'assets/dashboard-images/5.jpg',
      'assets/dashboard-images/6.jpg',
      'assets/dashboard-images/7.jpg',
      'assets/dashboard-images/8.jpg'
    ];

    this.legal = [
      'assets/dashboard-images/9.jpg',
      'assets/dashboard-images/10.jpg',
      'assets/dashboard-images/11.jpg',
      'assets/dashboard-images/12.jpg'
    ];

    // this.featuredProducts = [
    //   'https://picsum.photos/300/200?random',
    //   'https://picsum.photos/300/200?random',
    //   'https://picsum.photos/300/200?random',
    //   'https://picsum.photos/300/200?random'
    // ];

    // this.nutsAndNotes = [
    //   'https://picsum.photos/300/150?random',
    //   'https://picsum.photos/300/150?random',
    //   'https://picsum.photos/300/150?random',
    //   'https://picsum.photos/300/150?random'
    // ];

    // this.legal = [
    //   'https://picsum.photos/300/150?random',
    //   'https://picsum.photos/300/150?random',
    //   'https://picsum.photos/300/150?random',
    //   'https://picsum.photos/300/150?random'
    // ];

    const dummyBackorder: Backorder = {
      partNumber: 12250174,
      quantity: 3,
      price: 311,
      submittedAt: '10/25/2018'
    };

    const dummyOpenorder: Openorder = {
      poNumber: '0412190c91',
      orderTitle: 'Test Order',
      retail: 1234.56,
      createdAt: '3/26/2019',
      createdBy: 'VQUEVEDO',
    };
    
    const dummyOpenorder2: Openorder = {
      poNumber: '0213490c91',
      orderTitle: 'Big Order',
      retail: 623453.56,
      createdAt: '5/20/2018',
      createdBy: 'MTAMBORNINO',
    }

    const dummySubmittedorder: Submittedorder = {
      poNumber: '0412290c93',
      orderTitle: 'Test Order',
      retail: 1234.56,
      createdAt: '2/24/2019'
    };

    const monthlySpecial: Monthlyspecial = {
      name: 'Coastal Moto',
      percentage: 5,
    };

    const monthlySpecial2: Monthlyspecial = {
      name: 'Monthly Special With a Longer Name Than Normal Special',
      percentage: 99,
    };

    const program: Program = {
      programName: '2019 Lock-In Tire Program',
      startDate: '1/2/2019',
      endDate: '1/31/2020'
    };

    const dummyCummulativeProgram = {
      name: 'Cummulative Helmet Program 2019',
      link: '#'
    };

    const dummyCummulativeProgram2 = {
      name: 'Cummulative Program With a Long Name Program 2020',
      link: '#'
    };

    this.monthlySpecials.push(monthlySpecial2);
    this.cummulativePrograms.push(dummyCummulativeProgram2);
    this.openOrders.push(dummyOpenorder2);
    for (let i = 0; i < 8; i++) {
      this.backOrders.push(dummyBackorder);
      this.openOrders.push(dummyOpenorder);
      this.submittedOrders.push(dummySubmittedorder);
      this.monthlySpecials.push(monthlySpecial);
      this.newPrograms.push(program);
      this.expiringPrograms.push(program);
      this.cummulativePrograms.push(dummyCummulativeProgram);
    }

  }

}

interface Backorder {
  partNumber: number;
  quantity: number;
  price: number;
  submittedAt: string;
}

interface Openorder {
  poNumber: string;
  orderTitle: string;
  retail: number;
  createdAt: string;
  createdBy: string;
}

interface Submittedorder {
  poNumber: string;
  orderTitle: string;
  retail: number;
  createdAt: string;
}

interface Monthlyspecial {
  name: string;
  percentage: number;
}

interface Program {
  programName: string;
  startDate: string;
  endDate: string;
}

interface CummulativeProgram {
  name: string;
  link: string;
}
