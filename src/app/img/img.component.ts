import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss']
})
export class ImgComponent implements OnInit {

  @Input() canZoom = false;
  @Input() src: string;
  @ViewChild('zoomBox') zoomBox: ElementRef;
  @ViewChild('zoomBoxEnlargedImage') zoomBoxEnlargedImage: ElementRef;
  @ViewChild('zoomBoxRefImage') zoomBoxRefImage: ElementRef;
  zoomImageSrc: string;

  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    if (this.canZoom) {
      this.zoomImageSrc = this.src.replace(/=400/g, '=800');
    }
  }

/**
  * Display zoombox and scroll image as user moves mouse over reference image
  * @param {MouseEvent} $event
  */
  displayZoomBox($event) {
    // Get cursor position
    const clientX = $event.offsetX;
    const clientY = $event.offsetY;

    // Calculate percentange of distance from left / top of image to cursor position
    const xPercentCoord = clientX / (this.zoomBoxRefImage.nativeElement.clientWidth) * 100;
    const yPercentCoord = clientY / (this.zoomBoxRefImage.nativeElement.clientHeight) * 100;

    // Make sure enlarged image is twice as large as reference image
    this.renderer.setStyle(this.zoomBoxEnlargedImage.nativeElement, 'width', `${this.zoomBoxRefImage.nativeElement.clientWidth * 2}px`);
    this.renderer.setStyle(this.zoomBoxEnlargedImage.nativeElement, 'height', `${this.zoomBoxRefImage.nativeElement.clientHeight * 2}px`);

    // Offset top and left values of enlarged image using percentage coordinates (this is what actually "moves" the image)
    this.renderer.setStyle(this.zoomBoxEnlargedImage.nativeElement, 'left', `${-xPercentCoord}%`);
    this.renderer.setStyle(this.zoomBoxEnlargedImage.nativeElement, 'top', `${-yPercentCoord}%`);
  }
}
