import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResultsComponent } from './search-results.component';
import { PartBadgeModule } from '../part-badge/part-badge.module';

@NgModule({
  declarations: [SearchResultsComponent],
  imports: [
    CommonModule,
    PartBadgeModule
  ],
  exports: [SearchResultsComponent]
})
export class SearchResultsModule { }
