
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileSubmittedOrderDetailsComponent } from './mobile-submitted-order-details.component';
import { PartCardMobileModule } from '../../part-card-mobile/part-card-mobile.module';
import { OrderHeaderModule } from  'src/app/order-header/order-header.module';

@NgModule({
  declarations: [MobileSubmittedOrderDetailsComponent],
  imports: [
    CommonModule, PartCardMobileModule, OrderHeaderModule
  ],
  exports: [ MobileSubmittedOrderDetailsComponent ]
})
export class MobileSubmittedOrderDetailsModule { }
