import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileSubmittedOrderDetailsComponent } from './mobile-submitted-order-details.component';

describe('MobileSubmittedOrderDetailsComponent', () => {
  let component: MobileSubmittedOrderDetailsComponent;
  let fixture: ComponentFixture<MobileSubmittedOrderDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileSubmittedOrderDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileSubmittedOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
