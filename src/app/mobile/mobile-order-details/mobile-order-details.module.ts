import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MobileOrderDetailsRoutingModule } from './mobile-order-details-routing.module';
import { MobileOrderDetailsComponent } from './mobile-order-details.component';
import { MobileOrderDetailsReviewModule } from '../mobile-order-details-review/mobile-order-details-review.module';
import { MobileSubmittedOrderDetailsModule } from '../mobile-submitted-order-details/mobile-submitted-order-details.module';

@NgModule({
  declarations: [MobileOrderDetailsComponent],
  imports: [
    CommonModule,
    MobileOrderDetailsReviewModule,
    MobileSubmittedOrderDetailsModule,
    MobileOrderDetailsRoutingModule,
  ],
  exports: [ MobileOrderDetailsComponent ]
})
export class MobileOrderDetailsModule { }
