import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MobileOrderDetailsComponent } from './mobile-order-details.component';
import { MobileOrderDetailsReviewComponent } from '../mobile-order-details-review/mobile-order-details-review.component';
import { MobileSubmittedOrderDetailsComponent } from '../mobile-submitted-order-details/mobile-submitted-order-details.component';

const routes: Routes = [
	{
		path: '',
		component: MobileOrderDetailsComponent
  },
  {
    path: 'review',
    component: MobileOrderDetailsReviewComponent
  },
  {
    path: 'submitted',
    component: MobileSubmittedOrderDetailsComponent
  }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MobileOrderDetailsRoutingModule { }
