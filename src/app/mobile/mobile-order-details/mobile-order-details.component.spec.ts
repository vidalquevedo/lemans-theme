import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileOrderDetailsComponent } from './mobile-order-details.component';

describe('MobileOrderDetailsComponent', () => {
  let component: MobileOrderDetailsComponent;
  let fixture: ComponentFixture<MobileOrderDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileOrderDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
