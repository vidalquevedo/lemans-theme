import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MobileRoutingModule } from './mobile-routing.module';
import { MobileComponent } from './mobile.component';
import { NavbarHeaderModule } from '../navbar-header/navbar-header.module';

@NgModule({
  declarations: [MobileComponent],
  imports: [
    CommonModule,
    NavbarHeaderModule,
    MobileRoutingModule
  ]
})
export class MobileModule { }
