import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileOrderDetailsReviewComponent } from './mobile-order-details-review.component';
import { PartCardMobileModule } from 'src/app/part-card-mobile/part-card-mobile.module';
import { OrderHeaderModule } from  'src/app/order-header/order-header.module';

@NgModule({
  declarations: [MobileOrderDetailsReviewComponent],
  imports: [
    CommonModule, PartCardMobileModule, OrderHeaderModule
  ],
  exports: [ MobileOrderDetailsReviewComponent ]
})
export class MobileOrderDetailsReviewModule { }
