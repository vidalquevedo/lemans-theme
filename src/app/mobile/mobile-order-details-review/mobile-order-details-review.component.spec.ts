import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileOrderDetailsReviewComponent } from './mobile-order-details-review.component';

describe('MobileOrderDetailsReviewComponent', () => {
  let component: MobileOrderDetailsReviewComponent;
  let fixture: ComponentFixture<MobileOrderDetailsReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileOrderDetailsReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileOrderDetailsReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
