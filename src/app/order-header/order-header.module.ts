import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderHeaderComponent } from './order-header.component';

@NgModule({
  declarations: [OrderHeaderComponent],
  imports: [
    CommonModule
  ],
  exports: [OrderHeaderComponent],
})
export class OrderHeaderModule { }
