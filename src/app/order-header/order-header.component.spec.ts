import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartOrderHeaderMobileComponent } from './part-order-header-mobile.component';

describe('PartOrderHeaderMobileComponent', () => {
  let component: PartOrderHeaderMobileComponent;
  let fixture: ComponentFixture<PartOrderHeaderMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartOrderHeaderMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartOrderHeaderMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
