import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-order-header',
  templateUrl: './order-header.component.html',
  styleUrls: ['./order-header.component.scss']
})
export class OrderHeaderComponent implements OnInit {
  @Input() readonly: boolean;

  @Output() toggleShowManageWarehouseTable: EventEmitter<boolean> = new EventEmitter();
  showManageWarehouseTable = false;

  constructor() { }

  ngOnInit() {
  }

  emitToggleShowManageWarehouseTable() {
    this.showManageWarehouseTable = !this.showManageWarehouseTable;
    this.toggleShowManageWarehouseTable.emit(this.showManageWarehouseTable);
  }

}
