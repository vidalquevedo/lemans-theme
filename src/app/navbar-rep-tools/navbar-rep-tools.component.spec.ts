import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarRepToolsComponent } from './navbar-rep-tools.component';

describe('NavbarRepToolsComponent', () => {
  let component: NavbarRepToolsComponent;
  let fixture: ComponentFixture<NavbarRepToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarRepToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarRepToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
