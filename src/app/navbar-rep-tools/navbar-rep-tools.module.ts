import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarRepToolsComponent } from './navbar-rep-tools.component';

@NgModule({
  declarations: [NavbarRepToolsComponent],
  imports: [
    CommonModule
  ],
  exports: [NavbarRepToolsComponent]
})
export class NavbarRepToolsModule { }
