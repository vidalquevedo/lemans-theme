import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar-rep-tools',
  templateUrl: './navbar-rep-tools.component.html',
  styleUrls: ['./navbar-rep-tools.component.scss']
})
export class NavbarRepToolsComponent implements OnInit {

  @Input() active = 'sales-summary';

  constructor() { }

  ngOnInit() {
  }

}
