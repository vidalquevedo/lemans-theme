(function () {

  /**
   * Enable a custom dropdown submenu, which is not supported by default by Bootstrap 4.
   * 
   * The submeny adjusts its size on window resize.
   */
  function enableDropdownSubmenu() {
    var timeoutHandler;
    $('.dropdown-menu a.dropdown-toggle').on('mouseenter', function (e) {
      let $el = $(this);
      let $parent = $el.offsetParent('.dropdown-menu');
      let $subMenu = $el.next('.dropdown-menu');

      // Close any open child dropdpws
      if (!$el.next().hasClass('show')) {
        $el.parents('.dropdown-menu').first().find('.show').removeClass('show');
      }

      if (!$parent.parent().hasClass('navbar-nav')) {
        let $catNavigation = $('.navbar-category');
        $el.next().css({
          'min-height': $parent.outerHeight(),
          'width': $catNavigation.outerWidth() - $parent.outerWidth() - 30,
          'top': -4,
          'left': $parent.outerWidth() - 6
        });
      }

      // Display parent and child dropdowns
      $el.parent('li').toggleClass('show');
      $subMenu.toggleClass('show');

      // When parent dropdown hides, also hide sub dropdown
      $el.parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-menu .show').removeClass('show');
      });

      // Hook to window.resize to resize child dropdown if open
      $(window).resize(function () {
        if (!timeoutHandler) {
          const $catNavigation = $('.navbar-category');
          let $parent = $('.dropdown-menu a.dropdown-toggle').offsetParent('.dropdown-menu');
          timeoutHandler = setTimeout(function () {
            $('.dropdown-menu.dropdown-menu-submenu.show').first().css({
              'width': $catNavigation.outerWidth() - $parent.outerWidth() - 30
            });
            timeoutHandler = null;
          }, 100);
        }
      })

      return false;
    });
  }

  /**
   * Enable popovers, since they are not enabled by Bootstrap 4 by default
   */
  function enablePopovers() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  /**
   * Enable navbar category on mobile, which is displayed within a modal
   */
  function enableNavbarCategoryMobile() {
    // Add classes based on type of .nav-link
    $('.modal-navbar-category .nav-link').each(function() {
      $this = $(this);
      if ($this.siblings('.navbar-nav').length) {
        $this.addClass('has-sublevel');
      }
    });

    // Add event handling on click for .btn-back and .nav-link
    $('.modal-navbar-category .btn-back').on('click', function(e) {
      e.preventDefault();
      $('.modal-navbar-category').removeClass('slide');
      $('.modal-navbar-category .navbar-nav').removeClass('show');
    });
    $('.modal-navbar-category .nav-link').on('click', function(e) {
      $this = $(this);
      if ($this.siblings('ul').length > 0) {
        e.preventDefault();
        $('.modal-navbar-category').addClass('slide');
        $this.siblings('.navbar-nav').addClass('show');
        $('.modal-title').html($this.text());
      } else {
        // Close modal
        $('.modal-navbar-category').removeClass('slide');
        $('.modal-navbar-category .navbar-nav').removeClass('show');
        $('.modal-navbar-category').modal('hide');
      }
    });
  }

  function enableTabsOnHover() {
    $('#v-pills-tab a').on('mouseover', function (e) {
      e.preventDefault()
      $(this).tab('show');
      console.log('here');
    })
  }

  // Main
  $(document).ready(function () {

    // Enable dropdown submenu
    enableDropdownSubmenu();

    // Enable popovers
    enablePopovers();

    // Enable navbar category on mobile
    enableNavbarCategoryMobile();

    enableTabsOnHover();

  });

})();
